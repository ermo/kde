Title: Multiple KDE PIM packages have been renamed
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2016-12-17
Revision: 2
News-Item-Format: 1.0
Display-If-Installed: kde/accountwizard
Display-If-Installed: kde/console
Display-If-Installed: kde/importwizard
Display-If-Installed: kde/pimsettingexporter
Display-If-Installed: kde/storageservicemanager

The following packages have been renamed for KDE Applications 16.12.x:

* kde/accountwizard         -> kmail-account-wizard
* kde/console               -> akonadi-calendar-tools
* kde/importwizard          -> akonadi-import-wizard
* kde/pimsettingexporter    -> pim-data-exporter
* kde/storageservicemanager -> pim-storage-service-manager

Please install the appropriate new package and *afterwards* uninstall the
matching old package.

For example:

1. Take note of any packages depending on kde/console :
cave resolve \!kde/console

2. Install kde/akonadi-calendar-tools:
cave resolve kde/akonadi-calendar-tools -x

3. Re-install the packages from step 1.

4. Uninstall kde/console
cave resolve \!kde/console -x

Do it in *this* order or you'll potentially *break* your system.
