Title: kde/kdav became a framework
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2020-07-12
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde/kdav

kdav was moved from KDE Applications to KDE Frameworks, which is reflected by
moving our package from kde to kde-frameworks. Please note that Frameworks use
a different versioning scheme, eg. 5.x.y instead of yy.mm.x, appearing like a
downgrade.

Please install kde-frameworks/kdav and *afterwards* uninstall kde/kdav.

1. Take note of any packages depending on kde/kdav:

cave resolve \!kde/kdav

2. Install kde-frameworks/kdav:

cave resolve kde-frameworks/kdav -x

3. Re-install the packages from step 1.

4. Uninstall kde/kdav

cave resolve \!kde/kdav -x

Do it in *this* order or you'll potentially *break* your system.
