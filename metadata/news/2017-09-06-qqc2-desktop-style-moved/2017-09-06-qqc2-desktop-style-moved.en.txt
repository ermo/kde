Title: kde/qqc2-desktop-style has been moved to kde-frameworks
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2017-09-06
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde/qqc2-desktop-style

Please install kde-frameworks/qqc2-desktop-style and *afterwards* uninstall
kde/qqc2-desktop-style.

There are currently no packages depending on kde/qqc2-desktop-style, so no
further steps are needed.
