# Copyright 2010 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2013, 2016, 2018-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake

export_exlib_phases src_prepare src_install pkg_preinst

MY_PNV=${PN}-opensource-src-${PV}

SUMMARY="Qt Creator is a new cross-platform integrated development environment for use with Qt."
HOMEPAGE="https://www.qt.io/"

DOWNLOADS="mirror://qt/official_releases/${PN/-/}/$(ever range 1-2)/${PV}/${MY_PNV}.tar.xz"

LICENCES="
    LGPL-2.1 LGPL-3
    BSD-3 [[ note = [ ClassView and ImageViewer plugins ] ]]
    GPL-3 [[ note = [ Qt Quick Desinger ] ]]
    MIT   [[ note = [ Front-end for C++ ] ]]
"

SLOT="0"
MYOPTIONS="
    webengine [[ description = [ Use QtWebEngine instead of a more basic viewer to display the docs  ] ]]
"

min_qt_ver=5.11.0

DEPENDENCIES="
    build:
        virtual/pkg-config
        dev-lang/python:*[>=3.5]
    build+run:
        dev-lang/clang:10
        dev-lang/llvm:10
        dev-libs/yaml-cpp
        dev-util/elfutils   [[ note = perfparser ]]
        kde-frameworks/syntax-highlighting:5
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/qtbase:5[>=${min_qt_ver}][gui][sql]
        x11-libs/qtdeclarative:5[>=${min_qt_ver}]
        x11-libs/qtquickcontrols:5[>=${min_qt_ver}]
        x11-libs/qtserialport:5[>=${min_qt_ver}]
        x11-libs/qtsvg:5[>=${min_qt_ver}]
        x11-libs/qttools:5[>=${min_qt_ver}]
        x11-libs/qtx11extras:5[>=${min_qt_ver}]
        x11-libs/qtxmlpatterns:5[>=${min_qt_ver}]
        webengine? ( x11-libs/qtwebengine:5[>=${min_qt_ver}] )
    suggestion:
        dev-util/qbs[>=1.12.1] [[
            description = [ Support for projects built with the qbs build system ]
        ]]
        dev-util/the_silver_searcher [[
            description = [ Used by a plugin as alternative to 'find in files' ]
        ]]
        (
            net-misc/openssh
            net-misc/rsync
        ) [[
            *description = [ Needed for remote deployment to linux targets ]
        ]]
        sys-devel/gdb[python] [[ description = [ For debugging with Qt Creator ] ]]
"

RESTRICT="test" # far to many tests need a running X server, disable building them below

UPSTREAM_DOCUMENTATION="http://doc.qt.io/${PN/-/}/index.html"
UPSTREAM_CHANGELOG="http://code.qt.io/cgit/${PN}/${PN}.git/tree/dist/changes-$(ever range 1-3).md"

ever is_scm || CMAKE_SOURCE=${WORKBASE}/${MY_PNV}

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_WITH_PCH:BOOL=FALSE
    # We don't want to build against Qt6 yet
    -DCMAKE_DISABLE_FIND_PACKAGE_Qt6:BOOL=TRUE
    -DClang_DIR="/usr/$(exhost --target)/lib/llvm/10/lib/cmake/clang"
    -DWITH_DOCS:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'webengine Qt5WebEngineWidgets'
)

DEFAULT_SRC_INSTALL_PARAMS=( INSTALL_ROOT="${IMAGE}"/usr/$(exhost --target) )

qt-creator_src_prepare() {
    cmake_src_prepare

    # Our llvm install layout is pretty annoying
    edo sed \
        -e '/set(CLANG_VERSION/ s/.${LLVM_VERSION_MINOR}.${LLVM_VERSION_PATCH}//' \
        -i src/libs/clangsupport/CMakeLists.txt

    edo sed \
        -e "s|\(DESTINATION \)share/icons|\1/usr/share/icons|" \
        -i src/plugins/coreplugin/CMakeLists.txt


    # Remove the bundled qbs
    edo rm -rf src/shared/qbs
}

qt-creator_src_install(){
    cmake_src_install

    # XXX: Unfortunately there is no way to override datadir. So simply re-organize folders here and
    # add awful symlinks because program location is used to locate resources.
    dodir /usr/share
    edo mv "${IMAGE}"/usr/$(exhost --target)/share/qtcreator "${IMAGE}"/usr/share/

    # Here is awful symlink
    dosym /usr/share/qtcreator /usr/$(exhost --target)/share/

    dosym /usr/$(exhost --target)/bin/qtcreator /usr/$(exhost --target)/bin/qt-creator

    # create .desktop file
    insinto /usr/share/applications
    hereins qtcreator.desktop <<EOF
[Desktop Entry]
Type=Application
Name=Qt Creator
Comment=Complete IDE for Qt applications
Exec=qtcreator
Icon=qtcreator
Categories=Qt;Development;IDE;
EOF
}

qt-creator_pkg_preinst() {
    # Make the update path work
    local dir="${ROOT}/usr/$(exhost --target)/share/qtcreator"
    if [[ -d "${dir}" && ! -L "${dir}" ]] ; then
            nonfatal edo rmdir "${dir}" || ewarn "removing ${dir} failed"
    fi
}

