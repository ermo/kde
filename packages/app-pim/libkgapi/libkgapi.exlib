# Copyright 2011, 2012 Johannes Nixdorf <mixi@exherbo.org>
# Copyright 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2015, 2018-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='qt' ]

export_exlib_phases src_prepare src_compile src_install

SUMMARY="An Akonadi Resource for Google services, like Google Calendar and Google Contacts"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="doc"

KF5_MIN_VER=5.71.0
QT_MIN_VER=5.13.0

DEPENDENCIES="
    build:
        dev-libs/libxslt
        doc? ( app-doc/doxygen )
    build+run:
        kde-frameworks/kcalendarcore:5[>=5.63.0]
        kde-frameworks/kcontacts:5[>=5.63.0]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        net-libs/cyrus-sasl
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
"

# Skip test which started failing with Qt 5.15
DEFAULT_SRC_TEST_PARAMS+=( ARGS+="-E contactfetchjobtest" )

libkgapi_src_prepare() {
    kde_src_prepare

    # Disable test which needs X
    edo sed -e "/add_libkgapi2_test(core accountmanagertest)/d" \
        -i autotests/CMakeLists.txt
}

libkgapi_src_compile() {
    default

    if option doc ; then
        edo doxygen "${CMAKE_SOURCE}"/doc/api/Doxyfile.local
    fi
}

libkgapi_src_install() {
    cmake_src_install

    if option doc ; then
        docinto html
        dodoc -r html/*
    fi
}

