# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Plasma libraries and utilities"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] FDL-1.2 GPL-2 LGPL-2.1"
SLOT="4"
MYOPTIONS="
    gps         [[ description = [ GPS support for geolocation ] ]]
    barcodes    [[ description = [ Create mobile barcodes from clipboard data via prison ] ]]
    file-search [[ description = [ Build the file search runner ] ]]
    geolocation [[ description = [ Data engine to provide location information via wifi or geoip ] ]]
    holidays    [[ description = [ Display holidays in the Plasma calendar ] ]]
    legacy-systray [[ description = [ Makes xembed systrays available in Plasma ] ]]
    qalculate   [[ description = [ Support advanced features of the calculator runner ] ]]
    userfeedback [[
            description = [ Allows sending anonymized usage information to KDE ]
    ]]

    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

if ever at_least 5.19.90 ; then
    KF5_MIN_VER="5.74.0"
    QT_MIN_VER="5.15.0"
else
    KF5_MIN_VER="5.70.0"
    QT_MIN_VER="5.14.0"
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        app-text/iso-codes
        kde/kscreenlocker[>=5.13.80]
        kde/kwin:${SLOT}
        kde/libksysguard:${SLOT}
        kde/libkworkspace[>=${PV}]
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}][daemon(+)]
        kde-frameworks/kactivities-stats:5[>=${KF5_MIN_VER}]
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kded:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdesu:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=5.7.0]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidletime:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpeople:5[>=${KF5_MIN_VER}]
        kde-frameworks/krunner:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwayland:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/libkscreen:5
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        media-libs/phonon[>=4.6.60][qt5(+)]
        sys-libs/zlib
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXau
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXfixes
        x11-libs/libXrender
        x11-libs/libxcb
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql] [[ note = [ runners/bookmarks ] ]]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        barcodes? ( kde-frameworks/prison:5[>=${KF5_MIN_VER}] )
        file-search? ( kde-frameworks/baloo:5[>=5.1.95] )
        gps? ( dev-libs/gpsd )
        geolocation? ( kde-frameworks/networkmanager-qt:5[>=${KF5_MIN_VER}] )
        holidays? ( kde-frameworks/kholidays:5 )
        legacy-systray? (
            x11-libs/libXtst
            x11-utils/xcb-util
            !dev-libs/xembed-sni-proxy [[
                description = [ xembed-sni-proxy has been merged into plasma-workspace ]
                resolution = uninstall-blocked-after
            ]]
        )
        qalculate? (
            sci-libs/libqalculate:=[>=2.0.0] [[ note = [ 2.0.0 to not need cln ] ]]
        )
        userfeedback? ( kde/kuserfeedback )
    run:
        kde/drkonqi
        kde/kactivitymanagerd
        kde/milou:${SLOT}          [[ note = [ Needed for krunner ] ]]
        kde/plasma-integration
        kde-frameworks/kinit:5
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        kde-frameworks/kquickcharts:5[>=${KF5_MIN_VER}]
        x11-apps/iceauth
        x11-apps/xmessage      [[ note = [ startkde, startplasma ] ]]
        x11-apps/xprop         [[ note = [ startkde, startplasma ] ]]
        x11-apps/xrdb          [[ note = [ startkde, startplasma ] ]]
        x11-apps/xsetroot      [[ note = [ startkde, startplasma ] ]]
        x11-libs/qtgraphicaleffects:5
        x11-libs/qtquickcontrols:5
        x11-libs/qttools:5         [[ note = [ startkde, startplasma need qtpaths, qdbus ] ]]
    post:
        kde/kde-cli-tools:${SLOT}  [[ note = [ Needs kde-open5, kcmshell5 ] ]]
        kde/plasma-desktop:4       [[ note = [ kapplymousetheme used in startkde ] ]]
    suggestion:
        (
            providers:elogind? ( sys-auth/elogind )
            providers:systemd? ( sys-apps/systemd[polkit] )
        ) [[ *description = [ Needed for shutdown/reboot functionality ] ]]
        dev-libs/libappindicator:0.1 [[ description = [ Needed for systray icons of GTK+3 based apps ] ]]
        dev-libs/libappindicator:0.1[gtk2] [[ description = [ Needed for systray icons of GTK+2 based apps ] ]]
        (
            kde/kwallet-pam:4
            net-misc/socat
        ) [[
            *description = [ Needed to automatically unlock kwallet when logging in ]
            *group-name = [ kwallet-autounlock ]
        ]]
        kde/kwayland-integration:${SLOT} [[
            description = [ Support for a wayland session ]
            group-name = [ wayland-session ]
        ]]
        kde/kdeplasma-addons:4 [[ description = [ Collection of new plasmoids ] ]]
        x11-apps/xdg-user-dirs [[ description = [ Create, manage and localize well known directories ] ]]
        x11-libs/appmenu-gtk-module [[
            description = [ Export menus of GTK+ apps to use with a global menu ]
        ]]
"

if ever at_least 5.19.90 ; then
    DEPENDENCIES+="
        build+run:
            kde/plasma-wayland-protocols
            kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
            kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
            media-libs/fontconfig
            media-libs/freetype:2
            sys-libs/wayland
            x11-libs/libXft
            x11-libs/qtsvg:5[>=${QT_MIN_VER}]
            x11-libs/qtwayland:5[>=${QT_MIN_VER}]
            x11-utils/xcb-util-image
            (
                media/pipewire[>=0.3]
                x11-dri/libdrm
            ) [[ note = [ automagic, wayland screen casting ] ]]
    "
else
    DEPENDENCIES+="
        build+run:
            legacy-systray? ( x11-utils/xcb-util-image )
        suggestion:
            x11-libs/qtwayland:5 [[
                description = [ Support for a wayland session ]
                group-name = [ wayland-session ]
            ]]
    "
fi

# NOTE: StatusNotifierItem and appmenu use a forked copy of libdbusmenu-qt
#       because of unfixed bugs in the upstream version.

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # While we have dev-libs/appstream packaged, we haven't set up any data
    # source, so building the runner based on it seems a bit pointless at
    # the moment.
    -DCMAKE_DISABLE_FIND_PACKAGE_AppStreamQt:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'legacy-systray xembed-sni-proxy' )
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'barcodes KF5Prison'
    'file-search KF5Baloo'
    'geolocation KF5NetworkManagerQt'
    'gps libgps'
    'holidays KF5Holidays'
    Qalculate
    'userfeedback KUserFeedback'
)
# We might bring the X option back sometime, but right now disabling it is broken.
#    'X X11'

# Wants to start an ioslave
DEFAULT_SRC_TEST_PARAMS+=( -E "testdesktop" )

plasma-workspace_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

