# Copyright 2013-2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdesdk.exlib', which is:
#     Copyright 2008-2011 Bo Ørsted Andresen

require kde-apps kde freedesktop-desktop gtk-icon-cache

export_exlib_phases src_prepare src_compile src_install pkg_postinst pkg_postrm

SUMMARY="A UML modelling tool and code generator"
DESCRIPTION="It can create diagrams of software and other systems in the industry-
standard Unified Modelling Language format, and can also generate code from UML
diagrams in a variety of programming languages."
HOMEPAGE+=" http://www.kde.org/applications/development/${PN}/"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        kde-frameworks/karchive:5
        kde-frameworks/kcompletion:5
        kde-frameworks/kconfig:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/kcrash:5
        kde-frameworks/kdelibs4support:5
        kde-frameworks/ki18n:5
        kde-frameworks/kiconthemes:5
        kde-frameworks/kio:5
        kde-frameworks/ktexteditor:5
        kde-frameworks/kwidgetsaddons:5
        kde-frameworks/kwindowsystem:5
        kde-frameworks/kxmlgui:5
        x11-libs/qtbase:5[>=5.2.0]
        x11-libs/qtsvg:5[>=5.2.0]
        x11-libs/qtwebkit:5[>=5.2.0]
    test:
        dev-lang/clang:*
        dev-lang/llvm:*
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Broken upstream, https://bugs.kde.org/show_bug.cgi?id=419202
    -DCMAKE_DISABLE_FIND_PACKAGE_Doxygen:BOOL=TRUE
    -DBUILD_KF5:BOOL=TRUE
    # It bundles kdev-php for PHP import. I have no idea why this sillyness
    # needs to happen with both projects being part of the same community
    # but until somebody needs it I don't plan to support it.
    -DBUILD_PHP_IMPORT:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'doc Doxygen' )
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DBUILD_UNITTESTS:BOOL=TRUE -DBUILD_UNITTESTS:BOOL=FALSE'
    '-DCMAKE_DISABLE_FIND_PACKAGE_LLVM:BOOL=FALSE -DCMAKE_DISABLE_FIND_PACKAGE_LLVM:BOOL=TRUE'
)

# Tests currently fail to build (16.12.0)
RESTRICT="test"

umbrello_src_prepare() {
    kde_src_prepare

    # QT_DOR_DIR is only defined with Qt4. What, umbrello still supports
    # building against that you ask. Welcome to the insane world of umbrello!
    edo sed -e "s|QT_DOC_DIR}/qch|KDE_INSTALL_QTQCHDIR}|" -i CMakeLists.txt
}

umbrello_src_compile() {
    default

    option doc && emake apidoc
}

umbrello_src_install() {
    cmake_src_install

    if option doc ; then
        edo pushd "${WORK}"/apidoc/html
        docinto html
        dodoc -r *
        edo popd
    fi
}

umbrello_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

umbrello_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

