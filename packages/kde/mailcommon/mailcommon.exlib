# Copyright 2016-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Library which provides support for mail applications"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    designer [[ description = [ Install Qt Designer plugins ] ]]
"

KF5_MIN_VER=5.71.0
QT_MIN_VER=5.13.0

DEPENDENCIES="
    build:
        dev-libs/libxslt   [[ note = [ xsltproc ] ]]
    build+run:
        kde/libkdepim[>=${PV}]
        kde/mailimporter[>=${PV}]
        kde/messagelib[>=${PV}]
        kde/pimcommon[>=${PV}]
        kde-frameworks/akonadi-contact:5[>=${PV}]
        kde-frameworks/akonadi-mime:5[>=${PV}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=5.63.0]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidentitymanagement:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmailtransport:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/kpimtextedit:5[>=${PV}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/syntax-highlighting:5[>=${KF5_MIN_VER}]
        media-libs/phonon[>=4.10.60][qt5(+)]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        designer? ( x11-libs/qttools:5[>=${QT_MIN_VER}] )
"

# NOTE: "if (KF5_USE_PURPOSE)...KF5::Purpose" in autotests, but it's never
#       searched

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'designer DESIGNERPLUGIN' )
CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Not sure if we can whitelist enough stuff for akonadi, which possibly
    # pulls in the need for a MySQL server as well
    -DKDEPIM_RUN_AKONADI_TEST:BOOL=FALSE
)

DEFAULT_SRC_TEST_PARAMS+=(
    # TODO: The encrypt test fails on b.k.o as well, figure out why the
     # other fails for us
    ARGS+="-E '(mailcommon-filter-filteractionencrypttest|mailcommon-filter-filteractiondecrypttest)'"
)

mailcommon_src_test() {
    xdummy_start

    esandbox allow_net "unix:${TEMP}/filteractionencrypttest-*/gpghome/S.gpg-agent"
    esandbox allow_net --connect "unix:${TEMP}/filteractionencrypttest-*/gpghome/S.gpg-agent"
    esandbox allow_net "unix:${TEMP}/filteractiondecrypttest-*/gpghome/S.gpg-agent"
    esandbox allow_net --connect "unix:${TEMP}/filteractiondecrypttest-*/gpghome/S.gpg-agent"

    test-dbus-daemon_run-tests

    esandbox disallow_net --connect "unix:${TEMP}/filteractiondecrypttest-*/gpghome/S.gpg-agent"
    esandbox disallow_net "unix:${TEMP}/filteractiondencrypttest-*/gpghome/S.gpg-agent"
    esandbox disallow_net --connect "unix:${TEMP}/filteractionencrypttest-*/gpghome/S.gpg-agent"
    esandbox disallow_net "unix:${TEMP}/filteractionencrypttest-*/gpghome/S.gpg-agent"

    xdummy_stop
}

