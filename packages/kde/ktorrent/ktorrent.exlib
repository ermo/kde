# Copyright 2008 Daniel Mierswa <impulze@impulze.org>
# Copyright 2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde gtk-icon-cache freedesktop-desktop

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="KTorrent - The KDE BitTorrent client"
DESCRIPTION="A rich featured BitTorrent client for KDE"

HOMEPAGE="https://www.kde.org/applications/internet/ktorrent/"

LICENCES="GPL-2"
SLOT="4"
MYOPTIONS="
    avahi
    ipfilter    [[ description = [ Filter IP addresses through a blocklist ] ]]
    mediaplayer [[ description = [ Phonon-based media player ] ]]
    plasma      [[ description = [ Allows you to shutdown your Plasma session when torrents finish ] ]]
    scripting   [[ description = [ Enables Kross scripting support ] ]]
    search      [[ description = [ Allows you to search for torrents ] ]]
    statistics  [[ description = [ Shows statistics about torrents in several graphs ] ]]
    syndication [[ description = [ Syndication plugin for KTorrent, supporting RSS and Atom ] ]]
    upnp        [[ description = [ Forward ports using UPnP ] ]]
"

KF5_MIN_VER=5.15.0
QT_MIN_VER=5.7.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        sys-devel/gettext
    build+run:
        dev-libs/boost
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        net-libs/GeoIP
        net-p2p/libktorrent[>=2.2]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtscript:5[>=${QT_MIN_VER}]
        avahi? ( kde-frameworks/kdnssd:5[>=${KF5_MIN_VER}] )
        ipfilter? ( kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}] )
        mediaplayer? (
            media-libs/phonon[qt5(+)]
            media-libs/taglib
        )
        plasma? (
            kde/libkworkspace
            kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
        )
        scripting? (
            kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
            kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
            kde-frameworks/kross:5[>=${KF5_MIN_VER}]
        )
        search? ( kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}] )
        statistics? ( kde-frameworks/kplotting:5[>=${KF5_MIN_VER}] )
        syndication? (
            kde-frameworks/syndication:5
            x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
        )
        upnp? ( kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}] )
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DENABLE_BWSCHEDULER_PLUGIN=ON
    -DENABLE_DOWNLOADORDER_PLUGIN=ON
    -DENABLE_INFOWIDGET_PLUGIN=ON
    -DENABLE_LOGVIEWER_PLUGIN=ON
    -DENABLE_MAGNETGENERATOR_PLUGIN=ON
    -DWITH_SYSTEM_GEOIP=ON

    # Commented out atm
    #-DENABLE_WEBINTERFACE_PLUGIN=ON
)

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DENABLE_SCANFOLDER_PLUGIN:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES+=(
    'avahi ZEROCONF_PLUGIN'
    'ipfilter IPFILTER_PLUGIN'
    'mediaplayer MEDIAPLAYER_PLUGIN'
    'plasma SHUTDOWN_PLUGIN'
    'scripting SCRIPTING_PLUGIN'
    'search SEARCH_PLUGIN'
    'statistics STATS_PLUGIN'
    'syndication SYNDICATION_PLUGIN'
    'upnp UPNP_PLUGIN'
)
# The above switches are enough, but to avoid the deps showing up
# under optional packages found in cmake's feature summary, disable
# searching for them as well.
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'avahi KF5DNSSD'
    'mediaplayer Phonon4Qt5'
    'mediaplayer Taglib'
    'plasma LibKWorkspace'
    'scripting KF5Kross'
    'statistics KF5Plotting'
    'syndication KF5Syndication'
)

ktorrent_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

ktorrent_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

