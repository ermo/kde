# Copyright 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2017-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ] gtk-icon-cache

SUMMARY="Juk is a jukebox, tagger and music collection manager"
HOMEPAGE+=" https://multimedia.kde.org/"

LICENCES="GPL-2 BSD-3 [[ note = [ cmake scripts ] ]]"
MYOPTIONS=""

KF5_MIN_VER=5.35.0
QT_MIN_VER=5.6.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        media-libs/phonon[qt5(+)][>=4.6.60]
        media-libs/taglib[>=1.6] [[ note = [ unreleased 1.9 is needed for Opus support ] ]]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
"

