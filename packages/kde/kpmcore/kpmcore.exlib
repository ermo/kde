# Copyright 2016-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde [ translations=ki18n ] \
    test-dbus-daemon

SUMMARY="KDE Partition Manager Core Library"
DESCRIPTION="
KDE Partition Manager is a utility program to help you manage the disk devices,
partitions and file systems on your computer. It allows you to easily create,
copy, move, delete, resize without losing data, backup and restore partitions.

KDE Partition Manager supports a large number of file systems,
including ext2/3/4, reiserfs, NTFS, FAT16/32, jfs, xfs and more.
"
DOWNLOADS="mirror://kde/stable/${PN}/${PV}/src/${PNV}.tar.xz"

LICENCES="GPL-3"
SLOT="4"
MYOPTIONS="
    ( linguas:
        ar ast bg bs ca ca@valencia cs da de el en_GB eo es et fi fr ga gl hr hu id is it ja ko lt
        lv mai mr nb nds nl nn pa pl pt pt_BR ro ru sk sl sr sr@ijekavian sr@ijekavianlatin
        sr@latin sv th tr ug uk zh_CN zh_TW
    )
"

QT_MIN_VER=5.10.0
KF5_MIN_VER=5.56.0

# Could not connect to DBus system bus, last checked: 4.0.0
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-crypt/qca:2
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        sys-apps/util-linux[>=2.33.2] [[ note = [ for libblkid, libuuid and sfdisk ] ]]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
    run:
        sys-apps/smartmontools
    suggestion:
        sys-fs/btrfs-progs       [[ description = [ Support for Btrfs filesystems ] ]]
        sys-fs/cryptsetup        [[ description = [ Support for LUKS encrypted volumes ] ]]
        sys-fs/dosfstools        [[ description = [ Support for FAT16/32 filesystems ] ]]
        sys-fs/f2fs-tools        [[ description = [ Support for F2FS filesystems ] ]]
        sys-fs/lvm2              [[ description = [ Support for LVM volumes ] ]]
        sys-fs/ntfs-3g_ntfsprogs [[ description = [ Support for NTFS filesystems ] ]]
        sys-fs/reiserfsprogs     [[ description = [ Support for ReiserFS filesystems ] ]]
        sys-fs/udftools          [[ description = [ Support for UDF filesystems ] ]]
        sys-fs/xfsprogs          [[ description = [ Support for XFS filesystems ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_COVERAGE:BOOL=FALSE
    -DPARTMAN_DUMMYBACKEND:BOOL=FALSE
    -DPARTMAN_SFDISKBACKEND:BOOL=TRUE
)

