# Copyright 2011,2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdeutils.exlib', which is:
#     Copyright 2008-2011 Bo Ørsted Andresen

require kde-apps kde [ translations='ki18n' ] freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Tool to analyze disk usage graphically"
HOMEPAGE="https://utils.kde.org/projects/${PN}/"
DESCRIPTION="
Filelight allows you to quickly understand exactly where your diskspace is
being used by graphically representing your file system as a set of concentric
segmented-rings. You can use it to locate hotspots of disk usage and then
manipulate those areas using a file manager.
"

KF5_MIN_VER="5.46.0"

LICENCES="FDL-1.2 GPL-2"
MYOPTIONS=""

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.9.0]
    test:
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
"

filelight_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

filelight_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

