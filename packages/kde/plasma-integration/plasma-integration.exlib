# Copyright 2016-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_prepare src_test

SUMMARY="Qt platform theme integration plugins for the Plasma workspaces"

LICENCES="LGPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="X"

if ever at_least 5.19.90 ; then
    KF5_MIN_VER="5.74.0"
    QT_MIN_VER="5.15.0"
else
    KF5_MIN_VER="5.70.0"
    QT_MIN_VER="5.14.0"
fi

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde/breeze:4[>=${PV}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwayland:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/libxcb
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        X? (
            x11-libs/libXcursor
        )
    run:
        kde-frameworks/qqc2-desktop-style[>=${KF5_MIN_VER}] [[
            note = [ Provides the style set by the shipped Qt Platform Theme ]
        ]]
    test:
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
    suggestion:
        fonts/noto [[ description = [ Default font configured by Plasma ] ]]
        fonts/Hack [[ description = [ Default monospace font configured by Plasma ] ]]
"

# Annoyingly upstream uses find_package() for the (optional at
# runtime) fonts which have no suitable cmake module or config
# file, meaning they can never be found. Disable this noise
# below.
CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_DISABLE_FIND_PACKAGE_FontNotoSans:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_FontHack:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'X X11' )

# Wants to start an ioslave
DEFAULT_SRC_TEST_PARAMS+=(
    ARGS+="-E '(kfiledialog_unittest|kdeplatformtheme_unittest|kfiledialogqml_unittes|khintssettings_unittest)'"
)

plasma-integration_src_prepare() {
    kde_src_prepare

    edo sed \
        -e "/qmltests COMMAND/s/qmltestrunner/qmltestrunner-qt5/" \
        -i autotests/CMakeLists.txt
}

plasma-integration_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

