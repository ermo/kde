# Copyright 2016-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require freedesktop-desktop freedesktop-mime

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Wizard to add and configure PIM accounts"

LICENCES="FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER=5.71.0
QT_MIN_VER=5.13.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde/libkdepim[>=${PV}]
        kde/libkleo[>=${PV}]
        kde/pimcommon[>=${PV}]
        kde-frameworks/akonadi-mime:5[>=${PV}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidentitymanagement:5[>=${PV}]
        kde-frameworks/kimap:5[>=${PV}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kldap:5[>=${PV}]
        kde-frameworks/kmailtransport:5[>=${PV}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kross:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qttools:5[>=${QT_MIN_VER}]    [[ note = [ Qt5UiTools ] ]]
        x11-misc/shared-mime-info[>=1.3]
"

kmail-account-wizard_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

kmail-account-wizard_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}

