# Copyright 2013-2014, 2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdesdk.exlib', which is:
#     Copyright 2008-2011 Bo Ørsted Andresen

require kde-apps kde [ translations='ki18n' ] freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Frontend for cachegrind"
DESCRIPTION="A profile data visualization tool, used to determine the most
time consuming execution parts of program."
HOMEPAGE+=" http://www.kde.org/applications/development/${PN}/"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS=""

KF5_MIN_VER="5.18.0"

DEPENDENCIES="
    build:
        dev-lang/python:*
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        x11-libs/qttools:5 [[ note = [ lconvert, lrelease (added to tarball) ] ]]
    build+run:
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.6.0]
    suggestion:
        dev-util/valgrind [[ description = [ Needed to generate profiling data via callgrind ] ]]
"

kcachegrind_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kcachegrind_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

