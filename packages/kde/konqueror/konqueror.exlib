# Copyright 2016-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="Advanced File Manager, Web Browser and Universal Viewing Application"

LICENCES="FDL-1.2 GPL-1.2 LGPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    tts  [[ description = [ Build a plugin to speak portions or all of a website ] ]]
"

KF5_MIN_VER=5.71.0
QT_MIN_VER=5.12.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}] [[ note = [ possibly optional ] ]]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdesu:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kinit:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        sys-libs/zlib
        x11-libs/libX11
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtscript:5[>=${QT_MIN_VER}]   [[ note = [ plugins/searchbar ] ]]
        x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        tts? ( x11-libs/qtspeech:5[>=5.4.0] )
    run:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5, kioclient5 ] ]]
    recommendation:
        kde/keditbookmarks [[
            description = [ Edit bookmarks via the correspondent menu ]
        ]]
        kde/kfind [[
            description = [ Tool to search for local files ]
        ]]
"

# Seems nobody cares about the tests, only 50% pass on b.k.o
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'tts Qt5TextToSpeech'
)

DEFAULT_SRC_TEST_PARAMS+=(
    # Skip tests which want to start an ioslave/alsa fail on b.k.o
    ARGS+="-E '(konqhtmltest|konqviewmgrtest|konqviewtest|webengine_partapi_test)'"
)

konqueror_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

konqueror_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

konqueror_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

