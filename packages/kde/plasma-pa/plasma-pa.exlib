# Copyright 2015 Niels Ole Salscheider <niels_ole@salscheider-online.de>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]

SUMMARY="Plasma applet for managing PulseAudio's volume settings"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] GPL-2 LGPL-2.1"
SLOT="4"
MYOPTIONS=""

if ever at_least 5.19.90 ; then
    KF5_MIN_VER="5.74.0"
    QT_MIN_VER="5.15.0"
else
    KF5_MIN_VER="5.66.0"
    QT_MIN_VER="5.14.0"
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=5.14.0]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        media-libs/libcanberra
        media-sound/pulseaudio[>=5.0.0]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][glib]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
    run:
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
    recommendation:
        (
            media-libs/libcanberra[pulseaudio]
            sound-themes/sound-theme-freedesktop
        ) [[
            *description = [ Required for volume feedback sounds ]
            *group-name = [ feedback-sounds ]
        ]]
"

# Use GSettings instead of GConf
CMAKE_SRC_CONFIGURE_PARAMS+=( -DUSE_GCONF:BOOL=FALSE )

