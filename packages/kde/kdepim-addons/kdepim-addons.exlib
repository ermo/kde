# Copyright 2016 Niels Ole Salscheider <olesalscheider@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

SUMMARY="KDE Personal Information Management - Addons"
HOMEPAGE="http://kdepim.kde.org/"

LICENCES="GPL-2"
MYOPTIONS="
    import [[ description = [
        Build plugins for akonadi-import-wizard to import from e.g. geary, evolution ]
    ]]
"

KF5_MIN_VER=5.71.0
QT_MIN_VER=5.13.0

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-crypt/gpgme[>=1.11.1][qt5]
        app-text/discount[>=2] [[ note =
            [ RECOMMENDED, but not actually optional, it's a small dep though ]
        ]]
        dev-libs/grantlee:5
        kde/calendarsupport[>=${PV}]
        kde/eventviews[>=${PV}]
        kde/grantleetheme[>=${PV}]
        kde/incidenceeditor[>=${PV}]
        kde/kdepim-apps-libs[>=${PV}]
        kde/kitinerary[>=${PV}]
        kde/kpkpass[>=${PV}]
        kde/libgravatar[>=${PV}]
        kde/libkdepim[>=${PV}]
        kde/libkleo[>=${PV}]
        kde/libksieve[>=${PV}]
        kde/mailcommon[>=${PV}]
        kde/mailimporter[>=${PV}]
        kde/messagelib[>=${PV}]
        kde/pimcommon[>=${PV}]
        kde-frameworks/akonadi-calendar:5[>=${PV}]
        kde-frameworks/akonadi-contact:5[>=${PV}]
        kde-frameworks/akonadi-mime:5[>=${PV}]
        kde-frameworks/akonadi-notes:5[>=${PV}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcalendarcore:5[>=5.63.0]
        kde-frameworks/kcalutils:5[>=${PV}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=5.63.0]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kholidays:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidentitymanagement:5[>=${PV}]
        kde-frameworks/kimap:5[>=${PV}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmailtransport:5[>=${PV}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpimtextedit:5[>=${PV}]
        kde-frameworks/kontactinterface:5[>=${PV}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktnef:5[>=${PV}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/prison:5[>=${KF5_MIN_VER}]
        kde-frameworks/syntax-highlighting:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtwebengine:5[>=${QT_MIN_VER}]
        import? ( kde/akonadi-import-wizard[>=${PV}] )
    test:
        app-crypt/gnupg
"

# korganizer/plugins/CMakeLists.txt: "#add_subdirectory(hebrew)"
#        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]

# The tests need a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'import KPimImportWizard'
)

