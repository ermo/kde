# Copyright 2019-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde test-dbus-daemon

export_exlib_phases src_prepare

SUMMARY="Plasma system settings and kded modules to handle Thunderbolt devices"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER="5.70"
QT_MIN_VER="5.10"

DEPENDENCIES="
    build:
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
    run:
        app-admin/bolt
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
"

plasma-thunderbolt_src_prepare() {
    kde_src_prepare

    # Disable kdedtest, which needs a running X server
    edo sed -e "/^add_subdirectory(kded)/d" -i autotests/CMakeLists.txt
}

