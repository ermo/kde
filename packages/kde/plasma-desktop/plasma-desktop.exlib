# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="Plasma desktop"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] FDL-1.2 GPL-2"
SLOT="4"
MYOPTIONS="
    file-search [[ description = [ Build the file search K Control module ] ]]
    ibus        [[ description = [ Build the Emoji Picker and kimpanel, an input widget, and their IBus backend ] ]]
    input-kcm   [[ description = [ Configure input devices with the input KC(ontrol)M(odule) ] ]]
    synaptics   [[ description = [ Build KCM to configure Synaptics touchpads ] ]]
    ( providers: eudev systemd ) [[
        *description = [ udev provider ]
        number-selected = exactly-one
    ]]
"

if ever at_least 5.19.90 ; then
    MYOPTIONS+="
        open-desktop [[ description = [ OpenDesktop integration plugin ] ]]
    "

    KF5_MIN_VER=5.74.0
    QT_MIN_VER=5.15.0
else
    KF5_MIN_VER=5.69.0
    QT_MIN_VER=5.12.0
fi

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
        input-kcm? (
            x11-drivers/xf86-input-evdev[>=2.8.99.1]
            x11-drivers/xf86-input-libinput
        )
        synaptics? ( x11-server/xorg-server [[ note = [ xserver-properties.h ] ]] )
    build+run:
        kde/breeze:4[>=$(ever range -3)]
        kde/kwin:${SLOT}
        kde/libksysguard:4
        kde/libkworkspace[>=${PV}]
        kde/plasma-workspace:${SLOT}[>=${PV}]
        kde-frameworks/attica:5[>=${KF5_MIN_VER}]
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
        kde-frameworks/kactivities-stats:5[>=${KF5_MIN_VER}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/kded:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/krunner:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kunitconversion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        x11-apps/xkeyboard-config
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXcursor
        x11-libs/libXfixes
        x11-libs/libXft
        x11-libs/libXi[>=1.2.0]
        x11-libs/libxkbcommon
        x11-libs/libxkbfile
        x11-libs/libXrender
        x11-libs/libXtst
        x11-libs/libxcb
        x11-libs/qtbase:5[>=${QT_MIN_VER}][sql] [[ note = [ sql for KActivitiesStats, will move to kactivities if ready ] ]]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        x11-utils/xcb-util-image
        x11-utils/xcb-util-keysyms
        x11-utils/xcb-util-wm
        file-search? ( kde-frameworks/baloo:5[>=${KF5_MIN_VER}] )
        ibus? (
            dev-libs/glib:2
            inputmethods/ibus[>=1.5.0]
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        synaptics? (
            x11-drivers/xf86-input-synaptics
        )
        !kde-frameworks/kactivities:5[<5.20] [[
            description = [ The Activities KCM has been moved to plasma-desktop ]
            resolution = upgrade-blocked-before
        ]]
    run:
        kde/kde-cli-tools:${SLOT} [[ note = [ keditfiletype, kcmshell5 ] ]]
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        kde-frameworks/qqc2-desktop-style[>=${KF5_MIN_VER}]
        x11-apps/mkfontscale[>=1.2.0]
        x11-apps/setxkbmap
        x11-apps/xmodmap
        x11-apps/xrdb
        x11-libs/qtgraphicaleffects:5
        x11-libs/qtquickcontrols:5
        x11-libs/qtquickcontrols2:5
    suggestion:
        dev-libs/libunity [[
            description = [ Show application progress indicators for apps
                            supporting the Unity Launcher API ]
        ]]
        fonts/noto-emoji [[
            description = [ Font providing colored emoji for use with the Emoji Picker ]
        ]]
        kde/oxygen:4 [[
            description = [ Notifications sounds and Oxygen theme for KDE/Qt5 applications ]
        ]]
"

if ever at_least 5.19.90 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
            media-libs/phonon
            open-desktop? (
                kde/kaccounts-integration[>=20.04]
                net-libs/accounts-qt
            )
        run:
            open-desktop? ( net-libs/signon-plugin-oauth2 )
    "

    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        'open-desktop AccountsQt5'
        'open-desktop KAccounts'
    )
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kemoticons:5[>=${KF5_MIN_VER}]
            kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
            media-libs/fontconfig
            media-libs/freetype:2
    "
fi

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Hard disable the scim backend for kimpanel, since scim upstream seems
    # dead, prefer the newer ibus backend
    -DCMAKE_DISABLE_FIND_PACKAGE_SCIM:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'file-search KF5Baloo'
    IBus
    'ibus GLIB2'
    'ibus GIO'
    'ibus GObject'
    'input-kcm Evdev'
    'input-kcm XorgLibinput'
    Synaptics
    'synaptics XorgServer'
)

DEFAULT_SRC_TEST_PARAMS+=(
    # Want to start an kioslave
    # lookandfeel-kcmTest fails upstream, too; commented on the review:
    # https://phabricator.kde.org/D28662
    ARGS+="-E '(foldermodeltest|positionertest|test_kio_fonts|lookandfeel-kcmTest)'"
)

plasma-desktop_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

plasma-desktop_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

plasma-desktop_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

