# Copyright 2013, 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations=ki18n ]
require gtk-icon-cache

SUMMARY="KDE sudoku game"
DESCRIPTION="
A logic-based symbol placement puzzle. The player has to fill a grid so that each column, row as
well as each square block on the game field contains only one instance of each symbol."
HOMEPAGE+=" http://kde.org/applications/games/${PN}/"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS=""

KF5_MIN_VER="5.46.0"
QT_MIN_VER="5.11.0"

DEPENDENCIES+="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde/libkdegames:5
        (
            x11-dri/glu
            x11-dri/mesa
        ) [[ note = [ Roxdoku 3D sudoku support ] ]]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
"

