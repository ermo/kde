# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde

SUMMARY="Server side API for the Wayland protocol"
DESCRIPTION="
The API is Qt-styled removing the needs to interact with a for a Qt developer
uncomfortable low-level C-API. For example the callback mechanism from the
Wayland API is replaced by signals; data types are adjusted to be what a Qt
developer expects, e.g. two arguments of int are represented by a QPoint or a
QSize."

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

if ever at_least 5.19.90 ; then
    KF5_MIN_VER="5.74.0"
    QT_MIN_VER="5.15.0"
else
    KF5_MIN_VER="5.70.0"
    QT_MIN_VER="5.14.0"
fi

DEPENDENCIES="
    build:
        kde/plasma-wayland-protocols
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5   [[ note = qhelpgenerator ]]
        )
    build+run:
        kde-frameworks/kwayland:5[>=${KF5_MIN_VER}]
        sys-libs/wayland[>=1.15]
        x11-dri/mesa
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtwayland:5[>=${QT_MIN_VER}]
    test:
        compositor/weston:*
"

if ever at_least 5.19.90 ; then
    DEPENDENCIES+="
        build:
            sys-libs/wayland-protocols[>=1.18]
    "
else
    DEPENDENCIES+="
        build:
            sys-libs/wayland-protocols[>=1.15]
    "
fi

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

# Wants to start a Wayland server/Weston
RESTRICT="test"

