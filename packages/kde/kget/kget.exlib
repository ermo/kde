# Copyright 2013-2014, 2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdenetwork.exlib', which is:
#     Copyright 2008-2009, 2010, 2011, 2012 Ingmar Vanhassel
#     Copyright 2008-2011 Bo Ørsted Andresen

require kde-apps kde
require freedesktop-desktop gtk-icon-cache
require test-dbus-daemon

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="A versatile and user-friendly download manager."

LICENCES="GPL-2 LGPL-2.1 FDL-1.2
    BSD-3 [[ note = [ cmake scripts ] ]]"

MYOPTIONS="
    bittorrent [[ description = [ Support for downloading torrents via libktorrent ] ]]
    mms        [[ description = [ Support for reading mms streams ] ]]
    plasma     [[ description = [ Allows shutting down after completed downloads ] ]]
    sqlite     [[ description = [ SQLite Backend for the KGet History Plugin ] ]]
"

KF5_MIN_VER="5.55.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        app-crypt/gpgme[>=1.7.0][qt5]
        app-crypt/qca:2[>=2.1.0][qt5(+)]
        dev-libs/boost
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.7.0][sql]
        bittorrent? ( net-p2p/libktorrent[>=2.1] )
        mms? ( media-libs/libmms )
        plasma? ( kde/libkworkspace )
        sqlite? ( dev-db/sqlite:3 )
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'bittorrent KF5Torrent'
    'mms LibMms'
    'plasma LibKWorkspace'
    Sqlite
)

# Tests fails since porting from Qt4 -> 5
DEFAULT_SRC_TEST_PARAMS+=( -E "schedulertest" )

kget_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kget_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

