# Copyright 2013-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdesdk.exlib', which is:
#     Copyright 2008-2011 Bo Ørsted Andresen

require kde.org [ subdir=${PN}/${PV}/src ] kde [ translations='ki18n' ]
require xdummy [ phase=test ]
require freedesktop-mime gtk-icon-cache

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="KDE hex editor for viewing and editing the raw data of files"

LICENCES="
    GPL-2 LGPL-2.1 FDL-1.2
"
SLOT="4"
MYOPTIONS="
    designer [[ description = [ Build plugins for the use with Qt Designer ] ]]
"

KF5_MIN_VER="5.48.0"
QT_MIN_VER="5.9.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5
        virtual/pkg-config
    build+run:
        app-crypt/qca:2[>=2.1.0][qt5(+)]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtscript:5[>=${QT_MIN_VER}][tools(+)]
        x11-misc/shared-mime-info[>=0.40]
        designer? ( x11-libs/qttools:5[>=${QT_MIN_VER}] )
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # This should probably be optionalised to some extent if somebody cares
    -DBUILD_OKTETAKASTENLIBS:BOOL=TRUE
    -DBUILD_KPARTSPLUGIN:BOOL=TRUE
    -DBUILD_DESKTOPPROGRAM:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'designer DESIGNERPLUGIN' )

okteta_src_test() {
    xdummy_start

    default

    xdummy_stop
}

okteta_pkg_postinst() {
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

okteta_pkg_postrm() {
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

