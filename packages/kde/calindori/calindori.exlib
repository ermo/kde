# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde
require gtk-icon-cache

SUMMARY="Calendar application for Plasma Mobile"
DESCRIPTION="
Calindori is a touch friendly calendar application. It has been designed for
mobile devices but it can also run on desktop environments. Users of
Calindori are able to check previous and future dates and manage tasks and
events.
When executing the application for the first time, a new calendar file is
created that follows the ical standard. Alternatively, users may create
additional calendars or import existing ones."

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER="5.62.0"
QT_MIN_VER="5.14.0"

DEPENDENCIES="
    build+run:
        kde-frameworks/kcalendarcore:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
    run:
        kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
        x11-libs/qtquickcontrols2:5[>=${QT_MIN_VER}]
"

