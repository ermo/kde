# Copyright 2016-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="Common library for kdepim"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    designer [[ description = [ Install Qt Designer plugins ] ]]
    sharing  [[ description = [ Support for sharing files ] ]]
"

KF5_MIN_VER=5.71.0
QT_MIN_VER=5.13.0

DEPENDENCIES="
    build:
        dev-libs/libxslt   [[ note = [ xsltproc ] ]]
    build+run:
        kde/libkdepim[>=${PV}]
        kde-frameworks/akonadi-contact:5[>=${PV}]
        kde-frameworks/akonadi-search:5[>=${PV}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=5.63.0]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kimap:5[>=${PV}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kldap:5[>=${PV}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpimtextedit:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/purpose:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        designer? (
            kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
            x11-libs/qttools:5[>=${QT_MIN_VER}]
        )
    test:
        kde-frameworks/kmime:5[>=${PV}]
"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'designer DESIGNERPLUGIN' )

pimcommon_src_test() {
    xdummy_start

    default

    xdummy_stop
}

