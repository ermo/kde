# Copyright 2016-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require xdummy [ phase=test ]
require gtk-icon-cache

export_exlib_phases src_test

SUMMARY="A program that lets you write the computer equivalent of sticky notes"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    FDL-1.2 GPL-2 LGPL-2.1
"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER=5.71.0
QT_MIN_VER=5.13.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        dev-libs/libxslt   [[ note = [ xsltproc ] ]]
    build+run:
        dev-libs/grantlee:5[>=5.2]
        kde/grantleetheme[>=${PV}]
        kde/libkdepim[>=${PV}]
        kde/pimcommon[>=${PV}]
        kde-frameworks/akonadi-notes:5[>=${PV}]
        kde-frameworks/akonadi-search:5[>=${PV}]
        kde-frameworks/kcalendarcore:5[>=5.63.0]
        kde-frameworks/kcalutils:5[>=${PV}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=5.63.0]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdnssd:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kontactinterface:5[>=${PV}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpimtextedit:5[>=${PV}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/libX11
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
"

knotes_src_test() {
    xdummy_start

    default

    xdummy_stop
}

