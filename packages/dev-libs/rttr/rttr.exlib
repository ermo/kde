# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake

SUMMARY="An open source library, which adds reflection to C++."
DESCRIPTION="
RTTR stands for Run Time Type Reflection. It describes the ability of a
computer program to introspect and modify an object at runtime. It is also
the name of the library itself, which is written in C++ and released as open
source library.
The goal of this project is to provide an easy and intuitive way to use
reflection in C++."

HOMEPAGE="https://www.rttr.org/"
DOWNLOADS="${HOMEPAGE}releases/${PNV}-src.tar.gz"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}doc/master/classes.html"

# 32 tests fail
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # Would need boost
    -DBUILD_BENCHMARKS:BOOL=FALSE
    # Fails to build with gcc>=8 because the bundled rapidjson is missing
    # upstream fixes...
    -DBUILD_EXAMPLES:BOOL=FALSE
    -DBUILD_RTTR_DYNAMIC:BOOL=TRUE
    -DBUILD_STATIC:BOOL=FALSE
    -DBUILD_WITH_STATIC_RUNTIME_LIBS:BOOL=FALSE
    -DBUILD_PACKAGE:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=( 'doc DOCUMENTATION' )
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_UNIT_TESTS:BOOL=TRUE -DBUILD_UNIT_TESTS:BOOL=FALSE'
)

