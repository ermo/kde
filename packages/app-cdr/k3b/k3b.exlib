# Copyright 2008, 2009, 2010, 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="KDE CD Writing Software"
DESCRIPTION="
K3b is a CD and DVD burning application for Linux systems optimized for KDE. It
provides a comfortable user interface to perform most CD/DVD burning tasks,
such as creating an audio CD from a set of audio files or copying a CD. While
the experienced user can influence all steps of the burning process, the
beginner may find comfort in the automatic settings and the reasonable defaults
which allow a quick start. The actual burning is done by the command line
utilities cdrecord, cdrdao, and growisofs.
"
HOMEPAGE+=" http://www.k3b.org"

LICENCES="GPL-2 FDL-1.2"
SLOT="0"
MYOPTIONS="
    ffmpeg
    flac
    mp3 [[ description = [ Adds support for encoding mp3 files with lame, and decoding with mad ] ]]
    musepack
    sox [[ description = [ Support for encoding audio to various files formats ] ]]
    vorbis
    webkit [[ description = [ Show prettier information about discs with QtWebKit ] ]]
"

KF5_MIN_VER="5.56.0"
QT_MIN_VER="5.10.0"

DEPENDENCIES="
    build+run:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        kde/libkcddb:5[>=16.11.80]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kfilemetadata:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifyconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        media-libs/libdvdread[>=1.0]
        media-libs/libsamplerate
        media-libs/libsndfile
        media-libs/taglib[>=1.4.0]
        x11-libs/libX11
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtmultimedia:5 [[ note = [ Required for the K3b audio player ] ]]
        x11-misc/shared-mime-info[>=0.23]
        ffmpeg? ( media/ffmpeg )
        flac? ( media-libs/flac [[ note = [ Flac with C++ bindings ] ]] )
        mp3? (
            media-libs/libmad [[ note = [ MP3 audio decoder plugin ] ]]
            media-sound/lame [[ note = [ MP3 audio encoder plugin ] ]]
        )
        musepack? ( media-libs/libmpcdec )
        sox? ( media-sound/sox )
        vorbis? (
            media-libs/libogg
            media-libs/libvorbis
        )
        webkit? ( x11-libs/qtwebkit:5 )
    run:
        || (
            app-cdr/cdrtools
            app-cdr/cdrkit
        )
        app-cdr/cdrdao
        app-cdr/dvd+rw-tools[>=5.10]
        app-cdr/libburn [[ note = [ For cdrskin ] ]]
    suggestion:
        media-sound/normalize           [[ description = [ Normalize audio volume ] ]]
        media-video/transcode           [[ description = [ Support for transcoding video between different container formats ] ]]
        media-libs/vcdimager[xml][popt] [[ description = [ Support for dealing with VCD ] ]]
"
#   suggestion: media-video/emovix

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DK3B_BUILD_API_DOCS:BOOL=FALSE
    -DK3B_BUILD_EXTERNAL_ENCODER_PLUGIN:BOOL=TRUE
    # Note optional for libsamplerate, which is a hard dependency. Support for burning audio CDs.
    -DK3B_BUILD_SNDFILE_DECODER_PLUGIN:BOOL=TRUE
    -DK3B_BUILD_WAVE_DECODER_PLUGIN:BOOL=TRUE
    -DK3B_ENABLE_DVD_RIPPING:BOOL=TRUE
    # HAL breaks kde-4.6 and is not needed anymore
    -DK3B_ENABLE_HAL_SUPPORT:BOOL=FALSE
    # Only works with deprecated SLOT="2" and never did anything at all
    -DK3B_ENABLE_MUSICBRAINZ:BOOL=FALSE
    -DK3B_ENABLE_TAGLIB:BOOL=TRUE
    -DKDE_INSTALL_SYSCONFDIR:PATH=/etc
    -DENABLE_AUDIO_PLAYER:BOOL=TRUE
    # Optional dep for tests, unwritten
    -DCMAKE_DISABLE_FIND_PACKAGE_LibFuzzer:BOOL=TRUE
    # WIP in solid, disabled there
    -DWITH_NEW_SOLID_JOB:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'webkit Qt5WebKitWidgets'
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'ffmpeg K3B_BUILD_FFMPEG_DECODER_PLUGIN'
    'flac K3B_BUILD_FLAC_DECODER_PLUGIN'
    'mp3 K3B_BUILD_LAME_ENCODER_PLUGIN'
    'mp3 K3B_BUILD_MAD_DECODER_PLUGIN'
    'musepack K3B_BUILD_MUSE_DECODER_PLUGIN'
    'sox K3B_BUILD_SOX_ENCODER_PLUGIN'
    'vorbis K3B_BUILD_OGGVORBIS_DECODER_PLUGIN'
    'vorbis K3B_BUILD_OGGVORBIS_ENCODER_PLUGIN'
)

k3b_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

k3b_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

