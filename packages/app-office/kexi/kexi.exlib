# Copyright 2016-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir="${PN}/src" ] kde [ translations=ki18n ]
require freedesktop-desktop

SUMMARY="A visual database applications creator"
HOMEPAGE+=" http://www.kexi-project.org/"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] FDL-1.2 GPL-2 LGPL-2"
SLOT="0"
MYOPTIONS="
    examples
    msaccess   [[ description = [ Kexi MS Access migration driver ] ]]
    mysql      [[ description = [ Kexi MySQL migration driver ] ]]
    postgresql [[ description = [ Kexi PostgreSQL migration driver ] ]]
    webforms   [[ description = [ Allows creating web forms with the help of QtWebkit ] ]]
"
# NOTE: Support for migrating from another two DBMSs is commented out with
#       KEXI3 TODO add_subdirectory( migration ) in src/CMakeLists.txt.
#   freetds [[ description = [ Kexi Sybase migration driver ] ]]
#   xbase   [[ description = [ Kexi Xbase migration driver ] ]]

KF5_MIN_VER=5.7.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        dev-libs/kdb[>=3.1]
        dev-libs/kproperty[>=3.1]
        kde-frameworks/breeze-icons[binary-resource]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        office-libs/kreport[>=3.1][scripting]
        x11-libs/qtbase:5[>=5.4.0][gui]
        x11-libs/qttools:5[>=5.4.0]
        msaccess? ( dev-libs/glib:2 )
        mysql? ( virtual/mysql )
        postgresql? ( dev-db/postgresql-client )
        webforms? ( x11-libs/qtwebkit:5[>=5.4.0] )
        !app-office/calligra[<2.90][kde_parts:kexi] [[
            description = [ app-office/kexi was part of app-office/calligra in previous releases ]
            resolution = uninstall-blocked-after
        ]]
    suggestion:
        postgresql? ( dev-db/postgresql )
"

# 1 of 1 tests needs a running X server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Should be FindMarble. The relevant subdirectory
    # (src/plugins/forms/widgets/mapbrowser) is commented out anyway.
    -DCMAKE_DISABLE_FIND_PACKAGE_KexiMarble:BOOL=TRUE
    -DKEXI_DESKTOP_APP:BOOL=TRUE
    -DKEXI_MOBILE:BOOL=FALSE
    # Experimental
    -DKEXI_SCRIPTS_SUPPORT:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( EXAMPLES )
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'msaccess GLIB2'
    MySQL
    PostgreSQL
    'webforms Qt5WebKit'
    'webforms Qt5WebKitWidgets'
)

