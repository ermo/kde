# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir=calligra/${PV} ] kde

SUMMARY="A project management application"

HOMEPAGE="https://www.calligra.org/"

LICENCES="BSD-3 FDL-1.2 GPL-2"
SLOT="0"
MYOPTIONS="
    holidays    [[ description = [ Display public holidays ] ]]
    X
"

KF5_MIN_VER=5.45.0
QT_MIN_VER=5.9.0

DEPENDENCIES="
    build:
        dev-lang/perl:*
    build+run:
        app-crypt/qca:2[>=2.1.0]
        kde/kdiagram[>=2.6.0]
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/khtml:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kinit:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        holidays? ( kde-frameworks/kholidays:5[>=${KF5_MIN_VER}] )
        X? (
            x11-libs/libX11
            x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        )
        !app-office/calligra:2[<3.1.0] [[
            description = [ Plan has been split out from app-office/calligra ]
            resolution = uninstall-blocked-after
        ]]
"

# Disabled for now, will possibly be re-enabled in a future release:
# https://mail.kde.org/pipermail/release-team/2019-November/011594.html
#        addressbook? (
#            kde-frameworks/kcontacts:5
#            kde-frameworks/akonadi-contact:5
#        )
#        ical? ( kde-frameworks/kcalendarcore:5 )

# At least 5 out of 54 tests need a running display server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # support of multiple CPU architectures in one binary - needs Vc
    -DPACKAGERS_BUILD:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'holidays KF5Holidays'
    'X X11'
)

