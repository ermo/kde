# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require gtk-icon-cache

SUMMARY="A common plugin structure for various image applications"
HOMEPAGE="https://www.kde.org/"

LICENCES="
    BSD-2 [[ note = [ o2, https://github.com/pipacs/o2 ] ]]
    BSD-3 [[ note = [ cmake scripts ] ]]
    GPL-2
"
SLOT="0"

MYOPTIONS="
    ( kipi_plugins:
        mediawiki [[ description = [ Export images to MediaWiki ] ]]
        vkontakte [[ description = [ Export images to VKontakte.ru ] ]]
    )
"

KF5_MIN_VER="5.18.0"
QT5_MIN_VER="5.6.0"

DEPENDENCIES="
    build+run:
        kde/libkipi:5[>=15.12.0]   [[ note = [ aka 5.0.0 ] ]]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}] [[
            note = [ Flash export, possibly optional ]
        ]]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]   [[ note = [ possibly optional ] ]]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT5_MIN_VER}][gui]
        x11-libs/qtsvg:5[>=${QT5_MIN_VER}]
        x11-libs/qtxmlpatterns:5[>=${QT5_MIN_VER}]
        kipi_plugins:mediawiki? ( net-libs/libmediawiki )
        kipi_plugins:vkontakte? ( net-libs/libkvkontakte )
        !graphics/digikam[<5.0.0-beta5-r1] [[
            description = [ We unbundled kipi-plugins from graphics/digikam ]
            resolution = upgrade-blocked-before
        ]]
"
# TODO: Unbundle o2 (OAuth 2.0 for Qt, https://github.com/pipacs/o2), which
# has no install target yet. There's a pull request for it, though:
# https://github.com/pipacs/o2/pull/64

# NOTE: The following options turn up in the diff, because they are contained in
# o2's source, but kipi-plugins doesn't include o2's CMakeLists.txt, but just
# adds the sources it needs in common/libkipiplugins/CMakeLists.txt
#    -Do2_WITH_SPOTIFY:BOOL=FALSE
#    -Do2_WITH_SURVEYMONKEY:BOOL=FALSE
#    -Do2_WITH_KEYCHAIN'
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'kipi_plugins:mediawiki KF5MediaWiki'
    'kipi_plugins:vkontakte KF5Vkontakte'
)

