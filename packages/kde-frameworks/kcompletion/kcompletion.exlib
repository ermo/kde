# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='qt' ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="Provides Widgets with advanced auto-completion features"
DESCRIPTION="
This class offers \"auto-completion\", \"manual-completion\" or \"shell
completion\" on QString objects. A common use is completing filenames or URLs.
It can also be used for completing email-addresses, telephone-numbers,
commands, SQL queries, etc."

LICENCES="LGPL-2.1"
MYOPTIONS="
    designer [[ description = [ Install Qt designer plugins ] ]]
"

DEPENDENCIES="
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        designer? ( x11-libs/qttools:5[>=${QT_MIN_VER}] )
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Needed to build python bindings, no released consumers and we'd need
    # clang's python bindings and fix their and libclang's detection in the
    # cmake module, thus we disable it for now. Would need python,
    # clang[>=3.8], sip and PyQt5.
    -DCMAKE_DISABLE_FIND_PACKAGE_PythonModuleGeneration:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=(
    'designer DESIGNERPLUGIN'
)

kcompletion_src_test() {
    xdummy_start

    default

    xdummy_stop
}

