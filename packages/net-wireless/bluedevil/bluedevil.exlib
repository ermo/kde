# Copyright 2015 Timo Gurr <tgurr@exherbo.org>
# Copyright 2018-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require freedesktop-desktop freedesktop-mime

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Integrates the Bluetooth technology within KDE workspace and applications"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

if ever at_least 5.19.90 ; then
    KF5_MIN_VER="5.74.0"
    QT_MIN_VER="5.15.0"
else
    KF5_MIN_VER="5.70.0"
    QT_MIN_VER="5.14.0"
fi

DEPENDENCIES="
    build+run:
        kde-frameworks/bluez-qt:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kded:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-misc/shared-mime-info
    run:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
    recommendation:
        net-wireless/bluez[obex] [[ description = [ Support browsing, sending and receiving files ] ]]
    suggestion:
        media-sound/pulseaudio[bluetooth] [[ description = [ Support connections via the A2DP profile ] ]]
"

if ever at_least 5.19.90 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
    "
else
    DEPENDENCIES+="
        run:
            kde-frameworks/kirigami:2[>=${KF5_MIN_VER}]
    "
fi

bluedevil_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

bluedevil_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}

